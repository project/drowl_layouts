"use strict";

/**
* @file
* Preview image behavoir for layout settings fields.
*
*/
(function ($, Drupal) {
  Drupal.behaviors.drowl_layouts_settings = {
    attach: function (context, settings) {
      $('.form-wrapper--drowl-layouts-settings-preview', context).each(function () {
        var $form_item_wrapper = $(this);
        var $form_item = $(this).find('.form-item:first');
        // Selects
        if ($form_item.hasClass('js-form-type-select')) {
          var $form_item_value_preview = $form_item_wrapper.find('.form-item-value-preview:first');
          var $form_item_element = $form_item.find('.form-select:first');
          var form_item_initial_value = $form_item_element.val();
          // Set initial values
          $form_item_value_preview.attr('data-value-preview', form_item_initial_value);

          // Set column width for each column based on the initial value
          if ($form_item_value_preview.hasClass('column-widths')) {
            var initialValueWidths = form_item_initial_value.split('-');
            $form_item_value_preview.find('.layout-setting-preview__cell').each(function (i) {
              $(this).css('flex-basis', initialValueWidths[i] + '%');
            });
          }
          $form_item_element.on('change', function () {
            var currentValue = $(this).children('option:selected').val();
            $form_item_value_preview.attr('data-value-preview', currentValue);
            if ($form_item_value_preview.hasClass('column-widths')) {
              // Update column width for each column based on the current value
              var currentValueWidths = currentValue.split('-');
              $form_item_value_preview.find('.layout-setting-preview__cell').each(function (i) {
                $(this).css('flex-basis', currentValueWidths[i] + '%');
              });
            }
          });
        }
      });
    }
  };
})(jQuery, Drupal);