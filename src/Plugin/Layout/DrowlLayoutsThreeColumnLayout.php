<?php

namespace Drupal\drowl_layouts\Plugin\Layout;

use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Configurable two column layout plugin class.
 *
 * @internal
 *   Plugin classes are internal.
 */
class DrowlLayoutsThreeColumnLayout extends DrowlLayoutsMultiWidthLayoutBase implements PluginFormInterface {

  /**
   * Returns the number of columns in this layout.
   *
   * @var int
   */
  protected $columnCount = 3;

  /**
   * {@inheritdoc}
   */
  public function getColumnCount() {
    return $this->columnCount;
  }

  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [
      '33-33-33' => '33%/33%/33%',
      '50-25-25' => '50%/25%/25%',
      '25-25-50' => '25%/25%/50%',
      '25-50-25' => '25%/50%/25%',
    ];
  }

}
