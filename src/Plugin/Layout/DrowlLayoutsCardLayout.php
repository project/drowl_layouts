<?php

namespace Drupal\drowl_layouts\Plugin\Layout;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configurable two column layout plugin class.
 *
 * @internal
 *   Plugin classes are internal.
 */
class DrowlLayoutsCardLayout extends LayoutDefault implements PluginFormInterface {
  use DrowlLayoutsSettingsTrait {
    defaultConfiguration as drowlLayoutsSettingsTraitDefaultConfiguration;
    buildConfigurationForm as drowlLayoutsSettingsTraitBuildConfigurationForm;
    validateConfigurationForm as drowlLayoutsSettingsTraitValidateConfigurationForm;
    submitConfigurationForm as drowlLayoutsSettingsTraitSubmitConfigurationForm;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // Inherit from parents:
    $configuration = $this->drowlLayoutsSettingsTraitDefaultConfiguration();
    return $configuration + [
      // Set OUR defaults:
      'layout_variant' => 'card',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Inherit from parents:
    $form += $this->drowlLayoutsSettingsTraitBuildConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();
    // Add custom configuration:
    $form['layout_variant'] = [
      '#type' => 'select',
      '#title' => $this->t('Card variant'),
      '#options' => [
        'card' => $this->t('Card (vertical)'),
        'tile' => $this->t('Tile (overlayed)'),
      ],
      '#default_value' => $configuration['layout_variant'],
      '#required' => TRUE,
      '#description' => $this->t('Choose the variant of the card layout. Card = Image above the contents, Tile = Image as background of the contents.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-variant']],
      // Optionaly add the path to an SVG or HTML
      // file to have (dynamic) field value preview.
      // @todo Create card / tile preview html.
      // Note that we CANNOT Inject the extension path resolver, even if we try
      // to implement it like in https://www.drupal.org/docs/drupal-apis/services-and-dependency-injection/dependency-injection-in-plugin-block
      // which is pretty similiar, since they both use "PluginBase".
      '#field_setting_preview_markup' => \Drupal::service('extension.path.resolver')->getPath('module', 'drowl_layouts') . '/img/ico_column_width.svg',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->drowlLayoutsSettingsTraitValidateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->drowlLayoutsSettingsTraitSubmitConfigurationForm($form, $form_state);
    $this->configuration['layout_variant'] = $form_state->getValue('layout_variant');
  }

}
